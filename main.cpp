#include <iostream>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <vector>
#include "particle.h"
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
namespace mpi = boost::mpi;
using namespace std;


int main(int argc, char **argv){
 
	ifstream inst(argv[1], fstream::in);
	vector<Particle> m;
	in(m, inst);
	int n = atoi(argv[2]);

    mpi::environment env(argc, argv);
    mpi::communicator world;
    int d = world.size() - m.size()%world.size();
    m.resize(m.size() + d);
     
	while(n--){
		step(m, atoi(argv[3]), world);
	}
    
	return 0;
}
