#include "vector_math.h"
#include <math.h>
double len(const Vector_math &v){    
	return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}
Vector_math multConst(const Vector_math &v, double c){
	Vector_math res;
	res.x = v.x * c;
	res.y = v.y * c;
	res.z = v.z * c;
	return res;
}

Vector_math norm(const Vector_math &v){
	double r;
	r = len(v);
	return multConst(v, 1./r);
}



Vector_math sum(const Vector_math &v1, const Vector_math &v2){
	Vector_math res;
	res.x = v1.x + v2.x;
	res.y = v1.y + v2.y;
	res.z = v1.z + v2.z;
	return res;
}


Vector_math sub(const Vector_math &z, const Vector_math &p){
	Vector_math res;
	res.x = p.x - z.x;
	res.y = p.y - z.y;
	res.z = p.z - z.z;
	return res;
}

Vector_math invert(const Vector_math &v){
	return multConst(v, -1);
}

void zero(Vector_math &v){
	v.x = 0;
	v.y = 0;
	v.z = 0;
	return;
}

